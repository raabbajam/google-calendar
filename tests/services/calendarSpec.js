var expect = require('chai').expect;
var calendar = require('../../api/services/calendar');
var events = require('./events');
var fs = require('fs');
var write = fs.writeFileSync;
describe('calendar service', function () {
  describe('format', function () {
    it('should format using fullcalendar format', function (done) {
      var _events = calendar.format('fullCalendar', events);
      write(__dirname + '/eventFullCalendar.json', JSON.stringify(_events, null, 2));
      done();
    });
  });

});
