/**
* Calendar.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
    accessToken: { type: 'string', unique: true },
    refreshToken: { type: 'string', unique: true },
    user: { model: 'User', required: true },
  }
};
