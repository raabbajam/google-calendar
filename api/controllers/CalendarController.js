/**
 * CalendarController
 *
 * @description :: Server-side logic for managing calendars
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var gcal = require('google-calendar');
var Promise = require('promise');
var Calendar = {
	index: index,
	me: me,
};
function index(req, res) {
	// req.user = {id: 1, email: 'asdad@adsd.com'};
	if (!req.user)
		return res.redirect('/login');
	res.view({user: req.user.email});
}
function me(req, res) {
	var output = {};
	var _gCal;
	// req.user = {id: 1};
	if (!req.user)
		return res.redirect('/login');
	calendar.getToken(req.user)
		.then(function(token){
			return calendar.getCalendar(token);
		})
		.then(function(gCal){
			_gCal = gCal;
			return calendar.getAllList(_gCal);
		})
		.then(function(lists){
			var promises = lists.map(function (list) {
				return calendar.getAllEvents(_gCal, list);
			});
			return Promise.all(promises);
		})
		.then(function(events){
			events = calendar.format('fullCalendar', events);
			return res.json(events);
		})
		.catch(function (err) {
			console.log('Error', err.stack);
			return res.end(err.message);
		});
}

module.exports = Calendar;
