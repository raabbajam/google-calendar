var gcal     = require('google-calendar');
var moment = require('moment');
var formatInput = ['YYYY-MM-DD'];
var formatOutput = 'YYYY-MM-DD';
var Promise = require('promise');
var calendar = {
  getToken: getToken,
  getCalendar: getCalendar,
  getAllList: getAllList,
  getAllEvents: getAllEvents,
  format: format,
};

var formatters = {
  fullCalendar: fullCalendarFormmater,
};

function getToken(user) {
  console.log('userId', user.id);
  var options = {
    User: user.id,
    provider: 'google',
  };
  return Passport.find(options)
		.then(function (passport) {
			return passport[0].tokens.accessToken;
    });
}

function getCalendar(token) {
  return new gcal.GoogleCalendar(token);
}

function getAllList(gCal) {
  return new Promise(function(resolve, reject) {
    gCal.calendarList.list(function(err, calendarList) {
      if (err) return reject(err);
      return resolve(calendarList.items.map(function (list) {
        return list.id;
      }));
    });
  });
}
function getAllEvents(gCal, list) {
  return new Promise(function(resolve, reject) {
    gCal.events.list(list, function(err, events) {
      if (err) return reject(err);
      return resolve({
        key: list,
        value: events.items,
      });
    });
  });
}
function format(formatter, events) {
  return formatters[formatter](events);
}
function fullCalendarFormmater(events) {
  return events.reduce(function (all, event) {
    var _events = event.value.map(function (item) {
      var start = moment(item.start.dateTime || item.start.date, formatInput);
      var end = moment(item.end.dateTime || item.end.date, formatInput);
      return {
        title: item.summary || 'unknown event',
        start: start.format(formatOutput),
        end: end.format(formatOutput),
      };
    });
    return all.concat(_events);
  }, []);
}
module.exports = calendar;
